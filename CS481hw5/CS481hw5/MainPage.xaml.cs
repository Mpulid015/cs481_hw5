﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;

namespace CS481hw5
{//This class is for the picker
    public class Mypins
        {
        public string locName { get; set; }
        public Position Theposition { get; set; }
        public ObservableCollection<Mypins> fillList()
        {
            ObservableCollection<Mypins> sendlist = new ObservableCollection<Mypins>()
            {
                new Mypins(){locName = "Barra de Navida",Theposition = new Position(19.203, -104.683)},
                new Mypins(){locName="Guadalajara",Theposition = new Position(20.659698,-103.349609)},
                new Mypins(){locName="Mandalay Bay Convention Center",Theposition = new Position(36.086499654 ,-115.173165974)},
                new Mypins(){locName="Cabo San Lucas",Theposition=new Position(22.890533, -109.916740)},
                new Mypins(){locName = "The Pike",Theposition = new Position(33.759663628,-118.186832586)}

            };
            return sendlist;
        }
    }
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Mypins> MainList;
        public MainPage()
        {
            InitializeComponent();
            //Set the map to a satellite view at the beginning
            Mymap.MapType = Xamarin.Forms.Maps.MapType.Satellite;
            //This calls the function to set the pins to the map
            addingPins();
            //This will fill the observable collection for the picker
            MainList = new Mypins().fillList();
            Mypicker.ItemsSource = MainList;
            //This sets the map to show this area at the beginning
            MapSpan mapSpan = MapSpan.FromCenterAndRadius(new Position(32.715736, -117.161087), Distance.FromKilometers(4.0));
            Mymap.MoveToRegion(mapSpan);
        }
        //This will allow the user to change the view of the map
        void mapchange(object sender, EventArgs e)
        {
             var mapchange = (Button)sender;
         if(mapchange.Text == "Satellite")
         {
            Mymap.MapType = Xamarin.Forms.Maps.MapType.Satellite;
         }
         else if (mapchange.Text == "Street")
         {
                Mymap.MapType = Xamarin.Forms.Maps.MapType.Street;
            }
        }
        //This adds pins to the map
        private void addingPins()
        {
            Pin mypin1 = new Pin
            {
                Label = "Barra de Navidad",
                Address = "Jalisco\nfamily vacation spot",
                Type = PinType.Place,
                Position = new Position(19.203, -104.683)
            };
            Mymap.Pins.Add(mypin1);
            mypin1.MarkerClicked += async (s, args) =>
            {
                string theName = ((Pin)s).Label;
                string Thereason = ((Pin)s).Address;
                await DisplayAlert(theName, Thereason, "ok");
            };
            Pin mypin4 = new Pin
            {
                Label = "Guadalajara",
                Address = "Jalisco\nWhere my family is from",
                Type = PinType.Place,
                Position = new Position(20.659698,-103.349609)
            };
            Mymap.Pins.Add(mypin4);
            mypin4.MarkerClicked += async (s, args) =>
            {
                string theName = ((Pin)s).Label;
                string Thereason = ((Pin)s).Address;
                await DisplayAlert(theName, Thereason, "ok");
            };
            Pin mypin2 = new Pin
            {
                Label = "Mandalay Bay Convention Center ",
                Address = "3950 S Las Vegas Blvd, Las Vegas, NV 89119\nWhere I played in my first fighting game tournament",
                Type = PinType.Place,
                Position = new Position(36.086499654 ,-115.173165974)
            };
            Mymap.Pins.Add(mypin2);
            mypin2.MarkerClicked += async (s, args) =>
            {
                string theName = ((Pin)s).Label;
                string Thereason = ((Pin)s).Address;
                await DisplayAlert(theName, Thereason, "ok");
            };
            Pin mypin3 = new Pin
            {
                Label = "Cabo San Lucas",
                Address = " Cabo San Lucas, Baja California Sur, Mexico\nAnother Family vacation spot where we spent week at",
                Type = PinType.Place,
                Position = new Position(22.890533, -109.916740)
            };
            Mymap.Pins.Add(mypin3);
            mypin3.MarkerClicked += async (s, args) =>
            {
                string theName = ((Pin)s).Label;
                string Thereason = ((Pin)s).Address;
                await DisplayAlert(theName, Thereason, "ok");
            };
            Pin pin5 = new Pin
            {
                Label = "The Pike",
                Address = "The Pike Outlets, 95 S Pine Ave, Long Beach, CA 90802 \n The place I go to when I vists family in Long Beach",
                Type = PinType.Place,
                Position = new Position(33.759663628,-118.186832586)
                
            };
            Mymap.Pins.Add(pin5);
            pin5.MarkerClicked += async (s, arg) =>
            {
                string theName = ((Pin)s).Label;
                string Thereason = ((Pin)s).Address;
                await DisplayAlert(theName, Thereason, "ok");
            };

        }
        //This is the function for the picker so that when the user selects one area it will go to that place on the map
        public void OnPinselected(object sender,EventArgs e)
        {
            Picker Theselected = (Picker)sender;
            var thepin =(Mypins)Theselected.SelectedItem;
            MapSpan mapSpan = MapSpan.FromCenterAndRadius(thepin.Theposition, Distance.FromKilometers(4.0));
            Mymap.MoveToRegion(mapSpan);
        }
    }
    
}
